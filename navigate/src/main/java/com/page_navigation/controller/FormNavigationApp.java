package com.page_navigation.controller;

import com.page_navigation.dataAccess.FormPage1;
import com.page_navigation.dataAccess.FormPage2;
import com.page_navigation.dataAccess.FormPage3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FormNavigationApp extends Application{
    private Stage stg;
    private Scene page1Scene, page2Scene, page3Scene;

    private FormPage1 page1;
    private FormPage2 page2; 
    private FormPage3 page3;

    @Override
    public void start(Stage stg) {

        this.stg = stg;

        page1 = new FormPage1(this);
        page2 = new FormPage2(this);
        page3 = new FormPage3(this);

        page1Scene = new Scene(page1.getView(), 0400, 0300);
        page2Scene = new Scene(page2.getView(), 400, 300);
        page3Scene = new Scene(page3.getView(), 400, 300);

        stg.setScene(page1Scene);
        stg.setTitle("Form navigation");
        stg.show();
        
    }

    public void navigateToPage1(){

        page2.setField2Value(page2.getField2Value());
        page1.setField1Value(page1.getField1Value());
        stg.setScene(page1Scene);

    }

    public void navigateToPage2(){

        page1.setField1Value(page1.getField1Value());
        page3.setField3Value(page3.getField3Value());
        stg.setScene(page2Scene);

    }

    public void navigateToPage3(){

        page2.setField2Value(page2.getField2Value());
        stg.setScene(page3Scene);

    }


    
}
