package com.page_navigation;

import com.page_navigation.controller.FormNavigationApp;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) throws Exception{
        System.out.println("Hello world!");
        Application.launch(FormNavigationApp.class,args);
    }
}